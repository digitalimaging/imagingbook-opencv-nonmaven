
/**
 * This package contains classes for integrating OpenCV's Java build
 * with ImageJ. 
 * 
 * Installation: Place the containing JAR file in Image/jars
 * 
 * @author W. Burger
 */
package imagingbook.opencv;