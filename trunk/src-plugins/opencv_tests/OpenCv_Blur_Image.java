package opencv_tests;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import imagingbook.opencv.Convert;
import imagingbook.opencv.OpenCvLoader;


/**
 * Demo plugin showing the use of OpenCV with ImageJ.
 * Blurs the image (of any ImageJ type) and shows the result in a new image.
 * @author W. Burger
 * @version 2016/01/19
 */
public class OpenCv_Blur_Image implements PlugInFilter {
	
	static {
		OpenCvLoader.assureOpenCv();	// make sure OpenCV is loaded
	}	
	
	public int setup(String args, ImagePlus im) {
		return DOES_ALL + NO_CHANGES;
	}

	public void run(ImageProcessor ip) {
		Mat src = Convert.toMat(ip);
		
		// clone src - > dst
		Mat dst = src.clone();
//		Mat dst = new Mat(w, h, CvType.CV_8U);
		
		// blur the image
		double r = 10;
		Imgproc.blur(src, dst, new Size(r, r));
		
		// converting back
		ImageProcessor ip2 = Convert.toImageProcessor(dst);
		// showing result
		new ImagePlus("dst from OpenCV", ip2).show();
	}
}
