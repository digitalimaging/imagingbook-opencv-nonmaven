package opencv_tests;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.opencv.OpenCvLoader;

/**
 * This ImageJ plugin tests the OpenCV-ImageJ installation.
 * Assumes JAR and DLL files to be properly installed
 * (see installation instructions).
 * @author W. Burger
 * @version 2016/01/19
 */
public class OpenCv_Startup_Test implements PlugIn {
	
	@Override
	public void run(String arg0) {
		// IjLogStream.redirectSystem(); 	// requires import imagingbook.lib.ij.IjLogStream;
				
		if (!OpenCvLoader.assureOpenCv()) {
			System.out.println("OpenCV could not be loaded!");
			return;
		}
		
		IJ.log("OpenCV version = " + Core.VERSION);
		IJ.log("Number of CPUs = " + Core.getNumberOfCPUs());
		
		Mat mat = Mat.eye(3, 3, CvType.CV_8UC1);
		IJ.log("mat = \n" + mat.dump());
	}
	
	

}
