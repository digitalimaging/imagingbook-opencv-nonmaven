package imagingbook.opencv;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import ij.IJ;


/**
 * Utility class for loading OpenCV's libraries, i.e. a JAR
 * file and DLL file with the native code.
 * It is important that both library parts are loaded in the
 * scope of the same Java class loader. Most of the code in this
 * class is concerned with setting up a proper class loading
 * context.
 * TODO: 
 * 
 * @author W. Burger
 * @version 2016/01/19
 */
public abstract class OpenCvLoader {
	
//	private static final String OpenCvDirectory = IJ.getDirectory("imagej") + "opencv";
//	private static final String DllHelperJarName = "dllhelper.jar";
//	private static final String DllHelperClassName = "com.kaputnik.DllLoader";
//	private static final String IjPropertyKey = "OpenCvLoaderKey";
	
	private static final String IjDirectory = IJ.getDirectory("imagej");
	private static final String JarsDirectory = IjDirectory + "jars/";
	private static final String OpenCvDirectory = JarsDirectory + "opencv";
	
	private static final String DllHelperJarName = "opencv-ij.jar";
	private static final String DllHelperClassName = "imagingbook.opencv.DllLoader";
	
	private static final String IjPropertyKey = "OpenCvLoaderKey";
	
	private static boolean BeVerbose = false;

	/**
	 * Call this method to make sure that OpenCV's native library
	 * is properly loaded and running. The method loads the required
	 * native DLL if necessary.
	 * TODO: find a better test for OpenCV to be already loaded, polish this method
	 * 
	 * @return true if OpenCV is loaded and running, false otherwise
	 */
	public static boolean assureOpenCv() {
		IJ.log("in assureOpenCv...");
		if (!IJ.isWindows()) {
			IJ.error("OpenCV is currently available for Windows only!");
		}
		if (IJ.getProperty(IjPropertyKey) == null) {
			IJ.log("assureOpenCv: trying to load OpenCV");
			if (loadOpenCV()) {
				IJ.setProperty(IjPropertyKey, true);
				IJ.log("assureOpenCv: OpenCV load successful");
				return true;
			}
			else {	// load failed
				IJ.log("assureOpenCv: OpenCV load failed");
				return false;
			}
		}
		else {
			IJ.log("assureOpenCv: OpenCV already loaded");
			return true;
		}
	}
	
	/**
	 * Loads the native DLL for OpenCV. It searches for the loader
	 * class by name, thus no import of the loader class is required.
	 * Invocation of the loader is done by reflection.
	 * The associated JAR file is assumed to reside in ImageJ/jar/ 
	 * directory of the ImageJ installation.
	 */
	private static boolean loadOpenCV() {
		//System.out.println("LOADING OPENCV ++++++++++++++++++++");
		//ClassLoader extLoader = IJ.class.getClassLoader(); // works
		//ClassLoader extLoader = IJ.class.getClassLoader().getParent(); // works
		//ClassLoader extLoader = com.sun.nio.zipfs.ZipInfo.class.getClassLoader(); // = THE extension class loader (works)
		//System.out.println("extension loader = " + extLoader);
		
		
		String openCvJarName = FileUtils.findFile(OpenCvDirectory, "opencv", ".jar");
		URL opencvJarUrl = FileUtils.urlFromFile(new File(OpenCvDirectory, openCvJarName));
		IJ.log("loadOpenCV: opencvJarUrl = " + opencvJarUrl.toString());
		
		//URL dllhelperUrl = FileUtils.urlFromFile(new File(OpenCvDirectory, DllHelperJarName));
		URL dllhelperJarUrl = FileUtils.urlFromFile(new File(JarsDirectory, DllHelperJarName));
		IJ.log("loadOpenCV: dllhelperJarUrl = " + dllhelperJarUrl.toString());
		
		
		// configure the class loader:
		URLClassLoader loader = (URLClassLoader) IJ.class.getClassLoader();
		// add JARs to the class loader:
		FileUtils.addUrl(loader, opencvJarUrl);
		FileUtils.addUrl(loader, dllhelperJarUrl);
		//listUrls(loader);
		
		// load the generic DllLoader:
		Class<?> dllhelperclazz = null;
		try {
			dllhelperclazz = loader.loadClass(DllHelperClassName);
		} catch (ClassNotFoundException e) {
			throw new Error("[loadOpenCV] could not load class " + DllHelperClassName);
		}
		//System.out.println("found class: " + dllhelperclazz + " | loader = " + dllhelperclazz.getClassLoader());
		if (dllhelperclazz == null) 
			return false;
			
		// load the opencv DLL (expected to reside in sub-directory "x64" or "x86"):
		String openCvDllSubdir = null;
		switch(FileUtils.getOsBits()) {
		case(64):
			openCvDllSubdir = "/x64"; break;
		case(32):
			openCvDllSubdir = "/x86"; break;
		default:
			throw new Error("[loadOpenCV] Unknown OS architecture: " + FileUtils.getOsBits());
		}
		String openCvDllName = FileUtils.findFile(OpenCvDirectory + openCvDllSubdir, "opencv", ".dll");
		File openCvDllFile = new File(OpenCvDirectory + openCvDllSubdir, openCvDllName);
		String openCvDllPath = openCvDllFile.getAbsolutePath();
		IJ.log("loadOpenCV: openCvDllPath = " + openCvDllPath + " / " + openCvDllFile.exists());
		
		
		Object obj = null;
		//load the native library by invoking the static loadDllAbs() method:
//		try {
//			Method m = dllhelperclazz.getDeclaredMethod("loadDllAbs", String.class, boolean.class);
//			obj = m.invoke(null, openCvDllPath, BeVerbose);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//			throw new Error("[loadOpenCV] could not find/execute loadDllAbs() method");
//		}
		
		obj = DllLoader.loadDllAbs(openCvDllPath, BeVerbose);	// call statically linked method
		IJ.log("loadOpenCV: obj = " + obj.toString());
		
		return (obj != null);
	}
	
	// ---------------------------------------------------------------------
	
}
